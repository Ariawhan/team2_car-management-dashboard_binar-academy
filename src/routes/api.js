const router = require("express").Router();
const controller = require("../controller");
const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./src/public/images/uploads");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  },
});
const upload = multer({ storage });

// endpoint
router.post("/insert", upload.single("fileCar"), controller.insertCar);
router.put("/update/:id", upload.single("img"), controller.updateCar);
router.delete("/delete/:id", controller.deleteCar);
module.exports = router;
