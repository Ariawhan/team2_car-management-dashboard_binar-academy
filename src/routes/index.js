const router = require("express").Router();
const controller = require("../controller");
const path = require("path");

const api = require("./api");

//Page
router.get("/", (req, res) => {
  res.render("index");
});
router.get("/dashboard", controller.getCar);
router.get("/dashboard/update/:id", controller.getupdate);
router.get("/dashboard/add", (req, res) => {
  res.render("add");
});

//Api
router.use("/api", api);

module.exports = router;
