const { car } = require("../models");
const { Op } = require("sequelize");
const sequelize = require("sequelize");

const getupdate = async (req, res) => {
  idData = req.params.id;
  try {
    const carData = await car.findOne({
      where: {
        id: idData,
      },
    });
    res.render("../views/update", {
      status: true,
      message: "Cars Data",
      data: carData,
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      errors: [err.message],
    });
  }
};

const getCar = async (req, res) => {
  if (req.query.q !== undefined) {
    const data = req.query.q.toLowerCase();
    try {
      const carData = await car.findAll({
        where: sequelize.where(sequelize.fn("lower", sequelize.col("name")), "Like", "%" + data + "%"),
      });
      res.render("../views/dashboard", {
        status: true,
        message: "Cars Data",
        data: carData,
        query: true,
        search: data,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else if (req.query.p !== undefined) {
    const queryQ = req.query.p;
    try {
      const carData = await car.findAll({
        where: {
          size: queryQ,
        },
      });
      res.render("../views/dashboard", {
        status: true,
        message: "Cars Data",
        data: carData,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  } else {
    try {
      const carData = await car.findAll({});
      res.render("../views/dashboard", {
        status: true,
        message: "Cars Data",
        data: carData,
        addDataStatus: req.query.status,
      });
    } catch (err) {
      res.status(400).json({
        status: "fail",
        errors: [err.message],
      });
    }
  }
};

const updateCar = async (req, res) => {
  console.log("masuk");
  const idData = req.params.id;
  const data = {
    name: req.body.name,
    price: req.body.price,
    size: req.body.tipe,
    image: req.file.filename,
  };
  try {
    const carUpdate = await car.update(data, {
      where: {
        id: idData,
      },
    });
    if (parseInt(carUpdate) == 0) {
      console.log(parseInt(carUpdate));
      res.status(200).json({
        status: false,
        message: `Invalid id car ${id}`,
      });
    } else {
      console.log(carUpdate);
      res.status(200).json({
        status: true,
        message: "Car success edited",
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(400).json({
      status: "Error",
      errors: [err.message],
    });
  }
};

const insertCar = async (req, res) => {
  // console.log("Insert");
  const data = {
    name: req.body.name,
    price: req.body.price,
    size: req.body.tipe,
    image: req.file.filename,
  };
  try {
    const newCar = await car.create(data);
    res.status(201).json({
      status: true,
      message: "Car created successfully",
      data: {
        newCar,
      },
    });
  } catch (err) {
    console.log(err.message);
    res.status(400).json({
      status: "Error",
      errors: [err.message],
    });
  }
};

const deleteCar = async (req, res) => {
  try {
    const id = req.params.id;
    console.log(id);
    const deleted = await car.destroy({
      where: {
        id,
      },
    });
    if (deleted == 0) {
      console.log(deleted);
      res.status(200).json({
        status: false,
        message: `Invalid id car ${id}`,
      });
    } else {
      res.status(200).json({
        status: true,
        message: `Successful deleted car id : ${id}`,
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "Error",
      errors: [err.message],
    });
  }
};

module.exports = {
  getCar,
  insertCar,
  deleteCar,
  updateCar,
  getupdate,
};
