async function inputCar() {
  //console.log("input Car funcion");
  const data = {
    name: document.getElementById("input-name").value,
    price: document.getElementById("input-price").value,
    tipe: document.getElementById("input-tipe").value,
  };
  const images = document.getElementById("input-images").files;
  var formData = new FormData();
  formData.append("name", data.name);
  formData.append("price", data.price);
  formData.append("tipe", data.tipe);
  for (var i = 0; i < images.length; i++) {
    formData.append("fileCar", images[i]);
  }
  console.log(formData);
  const header = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  await axios
    .post("/api/insert", formData, header)
    .then(function (response) {
      // alert(response.status);
      if (response.status == 201) {
        window.location.href = "/dashboard?status=berhasil";
      } else {
        window.location.href = "/*";
      }
    })
    .catch(function (error) {
      alert(error);
    });
}

async function updateCar(idData) {
  //console.log("input Car funcion");
  const data = {
    name: document.getElementById("input-name" + idData).value,
    price: document.getElementById("input-price" + idData).value,
    tipe: document.getElementById("input-tipe" + idData).value,
  };
  const images = document.getElementById("input-img" + idData).files;
  var formData = new FormData();
  formData.append("name", data.name);
  formData.append("price", data.price);
  formData.append("tipe", data.tipe);
  for (var i = 0; i < images.length; i++) {
    formData.append("img", images[i]);
  }
  console.log(data);
  const header = {
    headers: {
      "content-type": "multipart/form-data",
    },
  };
  await axios
    .put("/api/update/" + idData, formData, header)
    .then(function (response) {
      // alert(response.status);
      if (response.status == 200) {
        window.location.href = "/dashboard";
      } else {
        window.location.href = "/*";
      }
    })
    .catch(function (error) {
      alert(error);
    });
}

async function deleteCar(idData) {
  await axios
    .delete("/api/delete/" + idData)
    .then(function (response) {
      // alert(response.status);
      if (response.status == 200) {
        window.location.href = "/dashboard";
      } else {
        window.location.href = "/*";
      }
    })
    .catch(function (error) {
      alert(error);
    });
}

async function search() {
  const search = document.getElementById("input-search").value;
  window.location.href = "/dashboard?q=" + search;
}

// async function medium() {
//   window.location.href = "/dashboard?p=Small";
// }
