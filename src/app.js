const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const routes = require("./routes");

const app = express();

// set view engine
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// set public static
// app.use(express.static("public"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public/images/uploads")));
app.use(express.static(path.join(__dirname, "controller")));

// set routes
app.use(routes);

//The 404 Route (ALWAYS Keep this as the last route)
app.get("*", function (req, res) {
  res.status(404).send("<h1>404</h1>");
});

module.exports = {
  app,
};
