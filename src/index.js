require("dotenv").config();
const morgan = require("morgan");
const { app } = require("./app");
const port = 3000;

app.use(morgan("combined"));
//Server Run
app.listen(port, () => {
  console.log(`Server running on ${Date(Date.now)}`);
  console.log(`Server listening on PORT: ${port}`);
});
