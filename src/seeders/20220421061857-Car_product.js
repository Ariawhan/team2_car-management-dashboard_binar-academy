'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert(
      "car",
      [
        {
          name: "F150 / Ford",
          price: "500000",
          size: "Small",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2022/03/21/120/718993/ford-patenkan-mode-drifting-di-mobil-listrik-dan-konvensional-bsk.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "X5 / BMW",
          price: "800000",
          size: "Large",
          image: "https://pict-a.sindonews.net/dyn/732/pena/news/2021/02/20/704/340952/di-tengah-terjangan-pandemi-covid19-new-bmw-x5-seharga-rp178-m-mengaspal-di-surabaya-bkg.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "MKZ / Lincoln",
          price: "900000",
          size: "Large",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2020/09/08/120/157664/jeep-grand-wagooner-siap-tantang-cadillac-esv-dan-lincoln-navigator-omc.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "M5 / BMW",
          price: "900000",
          size: "Large",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2021/09/18/120/543990/bmw-m5-competition-siap-jadi-investasi-masa-depan-buat-crazy-rich-indonesia-auk.jpeg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Fiesta / Ford",
          price: "900000",
          size: "Medium",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2022/03/25/120/723749/sukses-jiplak-mercedes-gclass-mobil-china-ini-akan-keluarkan-tiruan-ford-bronco-edd.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Accord / Honda",
          price: "900000",
          size: "Medium",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2021/08/19/120/515636/semua-honda-accord-di-thailand-dirilis-dengan-honda-sensing-thw.JPG",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "Navigator / Lincoln",
          price: "Small",
          size: "300000",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2022/02/09/120/681011/subaru-brz-cup-car-basic-siap-manjakan-adrenalin-para-pembalap-qho.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "LaCrosse / Buick",
          price: "1000000",
          size: "Large",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2022/02/13/120/684833/subaru-indonesia-100-persen-bawa-mobil-dari-jepang-bukan-malaysia-dan-thailand-ufa.jpg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
        {
          name: "X5 / BMW",
          price: "300000",
          size: "Large",
          image: "https://pict-b.sindonews.net/dyn/620/pena/news/2022/02/22/120/693311/bidik-crazy-rich-muda-ini-dia-bmw-yang-harganya-cuma-rp705-juta-ken.jpeg",
          createdAt: new Date().toISOString().split("T")[0],
          updatedAt: new Date().toISOString().split("T")[0],
        },
      ],
      {}
    );
  },


  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
