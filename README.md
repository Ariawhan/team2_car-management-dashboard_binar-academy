# Challenge Chapter 5 Car Management Dashboard

This Team Project Car Management Dashboard Copyright : Yuneda, Maria, Ramadhan Yudha, Ariawan, Aulia Project from Binar Academy with mentor [Imam Hermawan](https://gitlab.com/ImamTaufiqHermawan)

#### Develop by

- [Ariawhan](https://gitlab.com/Ariawhan)
- [Yuneda](https://gitlab.com/yuneda)
- [Ramadhan](https://gitlab.com/pratamaramadhan08)
- [Maria](https://gitlab.com/mnatalia283)
- [Aulia](https://gitlab.com/Aulia_utami)

## Mockup sistem

Bellow is the views made with Pug view engine and bootstrap :

![Alt-Text](/docs/ca-01.png)

![Alt-Text](/docs/ca-03.png)

## Entity Relationship Diagram

![Alt-Text](/docs/ERD.png)


## Module node.js

```
- npm install nodemon
- npm isntall express
- npm isntall morgan
- npm isntall axios
- npm isntall sequelize
- npm install sequelize-cli
- npm install pg
- npm install pg-hstore
- npm install pug
- npm isntall md5
- npm install body parser

```

## Configuration Database

```
# Creat Database
$ npm run create

## Migrate database
$ npm run migrate

### Deed Database
$ npm run seed
```

## Run Server
> $ npm run dev
